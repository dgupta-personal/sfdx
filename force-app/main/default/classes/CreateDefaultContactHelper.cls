public class CreateDefaultContactHelper {
    public static void createDefaultContact(List<Account> accounts){
        List<Contact> contactList = new List<Contact>();
        for(Account acc : accounts){
            contactList.add(new Contact(LastName='Default Contact',AccountId=acc.Id));
        }
        if(contactList.size() > 0){
            insert contactList;
        }
    }
}