@isTest
public class CreateDefaultContact_TEST {
	@isTest
    public static void method1(){
        Account acc = new Account();
        acc.Name = 'SFDX Account';
        insert acc;
        
        List<Contact> conForAcc = [Select Id from Contact Where AccountId =: acc.Id];
        System.assertEquals(1, conForAcc.size(), 'Default Contact Record Not Found For Account');
    }
}