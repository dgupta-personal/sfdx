trigger CreateDefaultContact on Account (after insert) {
    List<Account> accounts = Trigger.New;
    CreateDefaultContactHelper.createDefaultContact(accounts);
}